package mvc.util;

import java.util.Map;

public class XmlBean {
	private String actionname;
	private String actiontype;
	private String path;
	private String formtype;
	private Map<String, String> forward;

	public String getActionname() {
		return actionname;
	}

	public void setActionname(String actionname) {
		this.actionname = actionname;
	}

	public String getActiontype() {
		return actiontype;
	}

	public void setActiontype(String actiontype) {
		this.actiontype = actiontype;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFormtype() {
		return formtype;
	}

	public void setFormtype(String formtype) {
		this.formtype = formtype;
	}

	public Map<String, String> getForward() {
		return forward;
	}

	public void setForward(Map<String, String> forward) {
		this.forward = forward;
	}

	public String toString() {
		return "actionname=" + this.actionname + "||actiontype="
				+ this.actiontype + "||path=" + this.path + "||formtype="
				+ this.formtype + "||forward=" + forward.toString();
	}
}
