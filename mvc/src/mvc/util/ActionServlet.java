package mvc.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.action.Action;
import mvc.form.ActionForm;

public class ActionServlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public ActionServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String path = getpath(request.getServletPath());
		@SuppressWarnings("unchecked")
		Map<String, XmlBean> configmap = (Map<String, XmlBean>) this
				.getServletContext().getAttribute("configmap");
		XmlBean xmlb = configmap.get(path);
		ActionForm form = FullBean.full(xmlb.getFormtype(), request);
		Action action = loadAction(xmlb.getActiontype());
		String url = action.excute(request, response, form, xmlb.getForward());
		RequestDispatcher redis = request.getRequestDispatcher(url);
		redis.forward(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doGet(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

	private String getpath(String servletpath) {
		String path = servletpath.split("\\.")[0];
		return path;
	}

	private Action loadAction(String actiontype) {
		Action action = null;
		try {
			Class clazz = Class.forName(actiontype);
			action = (Action) clazz.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return action;
	}
}
