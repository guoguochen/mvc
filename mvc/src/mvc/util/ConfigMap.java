package mvc.util;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ConfigMap implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("信息：系统注销");

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext();
		String realpath = context.getInitParameter("struts-config");
		String tomcatepath = context.getRealPath("\\");
		Map<String, XmlBean> configmap = ConfigParse.parse(tomcatepath
				+ realpath);
		context.setAttribute("configmap", configmap);
		System.out.println("信息：系统加载完成");
	}

}
