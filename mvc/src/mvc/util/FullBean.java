package mvc.util;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import mvc.form.ActionForm;

public class FullBean {
	public static ActionForm full(String formtype, HttpServletRequest request) {
		ActionForm actionform = null;
		try {
			Class clazz = Class.forName(formtype);
			actionform = (ActionForm) clazz.newInstance();
			Field[] f_ar = clazz.getDeclaredFields();
			for (Field f : f_ar) {
				f.setAccessible(true);
				f.set(actionform, request.getParameter(f.getName()));
				f.setAccessible(false);
			}
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return actionform;
	}
}
