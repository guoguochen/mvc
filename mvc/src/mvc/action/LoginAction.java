package mvc.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.form.ActionForm;
import mvc.form.LoginForm;

public class LoginAction implements Action {

	@Override
	public String excute(HttpServletRequest request,
			HttpServletResponse response, ActionForm form,
			Map<String, String> mappingforward) {
		LoginForm lf = (LoginForm) form;
		String username = lf.getUsername();
		String password = lf.getPassword();
		boolean login = login(username, password);
		String loginmess = null;
		String url;
		if (login) {
			loginmess = username + "欢迎你！";
			url = mappingforward.get("success");
		} else {
			loginmess = "你的用户名：" + username + ",密码：" + password + "不正确，请确认";
			url = mappingforward.get("fail");
		}

		request.setAttribute("mess", loginmess);
		return url;
	}

	private boolean login(String username, String password) {
		if (username.equals("chen") && password.equals("123456"))
			return true;
		else
			return false;
	}

}
