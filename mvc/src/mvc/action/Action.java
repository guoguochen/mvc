package mvc.action;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mvc.form.ActionForm;

public interface Action {
	String excute(HttpServletRequest request, HttpServletResponse response,
			ActionForm form, Map<String, String> forward);
}
